BeAgency Lite
============

BeAgency is a very minimalistic yet creative WordPress theme for agencies and freelancers, which will help you make your project stand out from the crowd. Show off your latest photos and videos in all their glory with the responsive portfolio grid. Write blog posts with the beautiful, minimal blog. It is well documented which will help you to get your site running easy and fast.

--------

Features
--------

- Fully Responsive
- Isotope & Masonry Layouts
- Infinite Scroll
- WordPress Customizer Ready
- Font-Awesome Icons Retina Ready
- Translation Ready
- RTL Ready
- SEO Friendly
- Cross-Browser Compatibility
- Well Documented and Supported

-------------

Documentation
-------------

Theme documentation is available on http://docs.betheme.me.

---------

Changelog
---------

01/07/2016 V1.3.0
> Fixed Tag Cloud Widget styles.
> Upgraded Magnific Popup to 1.0.1.
> Upgraded imagesLoaded to 4.0.0.

12/16/2015 V1.2.1
> Removed Glyphicons Halflings from Bootstrap.
> Upgraded CMB2 to 2.1.2.
> Upgraded Custom Post Type Class to 1.4.

12/15/2015 V1.2.0
> Replaced TGM Plugin Activation for Theme Check compatibility.
> Upgraded Bootstrap to 3.3.6.
> Upgraded Font Awesome to 4.5.0.
> Upgraded Superfish to 1.7.7.
> Upgraded Isotope to 2.2.2.
> Upgraded SmoothScroll to 1.4.1.

11/26/2015 V1.1.2
> Fixed the height of header when WordPress Admin Bar is enabled.
> Added imagesLoaded for better experience when open Ajax portfolios.

11/17/2015 V1.1.1
> Switched markup for Comment Form and Contact Form to output valid HTML5.
> Added text editor for site footer. Now you can add HTML tags to footer.

11/15/2015 V1.1.0
> Improved Kirki.
> Customizer now supports child theme.
> Hidden the editor when the page using Home Page template.
> Changed the font size for Tag Cloud widget.

10/06/2015 V1.0.6
> Improved the notification when there is no posts found.
> Fixed search form issue.

09/30/2015 V1.0.5
> Fixed grid layout issue.

09/24/2015 V1.0.4
> Added style sheet for WordPress Visual Editor.

09/21/2015 V1.0.3
> Fixed the issue for textarea control (Customizer).

09/21/2015 V1.0.2
> Improved styles for Kirki.

09/20/2015 V1.0.1
> Fixed the issue when using PHP earlier than version 5.5.

09/15/2015 V1.0.0
> First released.